# Next steps

Future versions of BICI Add-on will focus on the following features:

## Business data in the algorithm

In order to have more relevant predictions on process which duration are more correlated to the business data, BICI Add-on should take in account some or all business data of these process.

## Several algorithms

The first version handle only one kind of prediction algorithm. In some cases, it could be more relevant to have different kind of algorithm depending on the process. BICI Add-on should let the user choose and configure algorithm by process.

## Predict case end flow

Right now BICI Add-on computes predictions about case duration. Another kind of predition could be the kind of state the case will finish in. The case could have a successful outcome when finishing on a specific path or an error outcome when finishing on an other path.
