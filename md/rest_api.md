# REST API

BICI Provide Rest apis to query BICI storage. 
A complete documentation using Swagger2 json format is available at url `http://<BICI_HOST>:<BICI_PORT>/v2/api-docs` 
